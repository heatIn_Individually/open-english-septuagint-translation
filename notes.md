# Open English Septuagint Translation Notes
## WEB New Testament Precedents
### Nouns
### Verbs
## WEB New Testament Verb Style
### Greek Verb Parsing
#### Aorist Indicative Active
[Acts 1:3](https://ebible.org/grcmt/ACT01.htm): παρέστησεν ἑαυτὸν ζῶντα "he showed himself alive"
#### Aorist Indicative Middle
[Acts 1:1](https://ebible.org/grcmt/ACT01.htm): ἐποιησάμην "I wrote"  
[Acts 1:2](https://ebible.org/grcmt/ACT01.htm): ἐξελέξατο "(whom) he had chosen"
#### Aorist Indicative Passive
[Acts 1:2](https://ebible.org/grcmt/ACT01.htm): ἀνελήφθη "[he] was received up"
#### Aorist Participle Middle, Nominative
[Acts 1:2](https://ebible.org/grcmt/ACT01.htm): ἐντειλάμενος "after he had given commandment"
#### Present Indicative Active
[Romans 1:6](https://ebible.org/grcmt/ROM01.htm): ἐστε "you are"
#### Present Infinitive Active
[Acts 1:1](https://ebible.org/grcmt/ACT01.htm): ποιεῖν "to do", διδάσκειν "to teach"
#### Future Indicative Middle
[Matthew 2:6](https://ebible.org/grcmt/MAT02.htm): ἐξελεύσεται "shall come"  
[Matthew 13:49](https://ebible.org/grcmt/MAT13.htm): ἐξελεύσονται "will come"
## Translator's Pocket Lexicon
### Greek Verbs
[G332](https://biblehub.com/greek/332.htm): ἀναθεματίζω "bind under a curse"  
[G782](https://biblehub.com/greek/782.htm): ἀσπάζομαι "salute"  
[G837](https://biblehub.com/greek/837.htm): αὐξάνω "increase"  
[G1398](https://biblehub.com/greek/1398.htm): δουλεύω "serve"  
[G1830](https://biblehub.com/greek/1830.htm).2: ἐξερημόω "lie desolate"  
[G1842](https://biblehub.com/greek/1842.htm): ἐξολοθρεύω "utterly destroy"  
[G2660](https://biblehub.com/greek/2660.htm): κατανύσσομαι “keep silence”  
[G2720](https://biblehub.com/greek/2720.htm): κατευθύνω "direct"
[G3000](https://biblehub.com/greek/3000.htm): λατρεύω "adore" with footnote "Greek λατρεύω (“offer divine service,” “service of worship,” reserved to God only)"  
[G3928](https://biblehub.com/greek/3928.htm): παρέρχομαι "pass by"  
[G4129](https://biblehub.com/greek/4129.htm): πληθύνω "multiply"  
[G4352](https://biblehub.com/greek/4352.htm)([BLB](https://www.blueletterbible.org/lexicon/g4352/lxx/lxx)): προσκυνέω "venerate" with footnote "proskuneo can also be translated “worship (in the broad sense),” “express adoration,” “fall down in reverence,” “do obeisance”"  
[G5091](https://biblehub.com/greek/5091.htm): τιμάω "honor"
[G5442](https://biblehub.com/greek/5442.htm): φυλάσσω "keep" (keep watch, keep ordinances, keep under watch, etc.)
### Greek Nouns
[G331](https://biblehub.com/greek/331.htm): ἀνάθεμα, ατος, τό "accursed"  
[G458](https://biblehub.com/greek/458.htm): ἀνομία, ας, ἡ "iniquity", "wickedness"  
[G1162](https://biblehub.com/greek/1162.htm): δέησις “petition,” “prayer” (for a specific, felt need)  
[G1345](https://biblehub.com/greek/1345.htm): δικαίωμα, ατος, τό "ordinance"  
[G1484](https://biblehub.com/greek/1484.htm): ἔθνος "nation"  
[G2906](https://biblehub.com/greek/2906.htm): κραυγή “cry”  
[G4009](https://biblehub.com/greek/4009.htm): πέρας "end (of the earth)"  
[G4188].2(https://biblehub.com/greek/4188.htm) παροικήσει "to do evil"  
[G5443](https://biblehub.com/greek/5443.htm): φυλή, -ῆς, ἡ "tribe"  
N/A: Εὐαῖος, -ου, ὁ "Hivite"  
N/A: Χαλδαῖος, -ου, ὁ "Chaldean"  
## Translator's Pocket Grammar
### Verbs
#### Tenses
##### Present
##### Imperfect
##### Future Tense
##### Aorist Tense
##### Perfect Tense
##### Pluperfect Tense
##### Future Perfect Tense
#### Moods
##### Indicative
Portrays events as ***factual*** ([Dongell](https://open.umn.edu/opentextbooks/textbooks/elementary-new-testament-greek)).  
"You are destroying."
##### Subjunctive
Expresses an action as ***uncertain*** or ***hypothetical*** ([Dongell](https://open.umn.edu/opentextbooks/textbooks/elementary-new-testament-greek)).  
"... if you are destroying" or "... so that you might be destroying"
##### Optative
Expresses a ***hope*** or a ***wish*** ([Dongell](https://open.umn.edu/opentextbooks/textbooks/elementary-new-testament-greek)).   
"Oh that you might be destroying..."
##### Imperative
Expresses a ***command*** ([Dongell](https://open.umn.edu/opentextbooks/textbooks/elementary-new-testament-greek)).  
"(You must) destroy...!"
#### Voices
##### Active
>  the persons involved (I, you, he/she/ it, we, y’all, they) are the ones who perform or carry out the action in question.

From [Elementary New Testament Greek by Joseph R. Dongell](https://open.umn.edu/opentextbooks/textbooks/elementary-new-testament-greek)
##### Middle
 > Verbs in the Middle Voice imply that the persons involved (I, you, he/she/it, we, y’all, they) are performing the action, but are somehow more intimately involved in the action than usual. This foggy notion must be teased out circumstance by circumstance, with a fair amount of interpretive wiggle room remaining.  

From [Elementary New Testament Greek by Joseph R. Dongell](https://open.umn.edu/opentextbooks/textbooks/elementary-new-testament-greek)
##### Passive
> The persons involved (I, you, he/she/it, we, y’all, they) are acted upon by someone or something else.

From [Elementary New Testament Greek by Joseph R. Dongell](https://open.umn.edu/opentextbooks/textbooks/elementary-new-testament-greek)
## Translator's Notes
### General Notes
Genesis 15:2: The MT says "אֲדֹנָ֤י יֱהוִה֙" (Adonai Yahweh), and Brenton's LXX says "Δέσποτα Κύριε" (Despota Kyrie). What's interesting about it is that Adonai was typically used in the Hebrew as the sort of "euphemism" for Yahweh, and Kyrios was the translation of Adonai. To add to the confusion, some of the passages translated as "Lord" in the NT use the word Despota. So to read the passage in Greek in light of this context inclines one to read it something like "Lord Lord." The closest thing I can find in the New Testament is Jude 1:4, where there is the construction "δεσπότην θεόν και κύριον", "Master, God, and Lord".