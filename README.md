# Open English Septuagint Translation

An English translation of the Septuagint Old Testament. Based most heavily on the [Brenton Greek LXX](https://ebible.org/grcbrent/) and [World English Bible](https://ebible.org/web/), with the [LXX2012](https://ebible.org/eng-lxx2012/) for reference. Not affiliated with the Open English Bible.

## Name
Open English Septuagint Translation

## Description
In the English-speaking world, there are many translations of the Christian scriptures. However, there are few options for Christians who see the Greek Septuagint (LXX) as the primary authoritative basis for the Old Testament. The goal of this project is to create a Old Testament that balances the readability, New Testament-continuity, and Public Domain accessbility of the [World English Bible](https://ebible.org/web/) with the Septuagint-basis that the [Brenton Greek LXX](https://ebible.org/grcbrent/) affords it.
The general translation method will be to start with a verse from the WEB, compare it to its Hebrew Masoretic Text basis to see how different words and clauses have been translated, compare those words and clauses to the existing WEB New Testament, compare the verse to the Brenton LXX, then modify it to fit the Septuagint. Since punctuation can be language- and manuscript-dependent, the project will attempt to retain the WEB punctuation and syntax as much as possible without compromising faithfulness to the Septuagint.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Usage
The project is edited as a set of [USFM](https://ubsicap.github.io/usfm/about/index.html) files, and will work in Bible translation software such as [Bibledit](https://bibledit.org/) and [Paratext](https://paratext.org/).

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
### Books by Length
~~Obadiah~~  
~~Haggai~~  
~~Nahum~~  
Jonah  
Zephaniah  
Malachi  
Habakkuk  
Joel  
Ruth  
Micah  
Song of Solomon  
Amos  
Lamentations  
Hosea  
Zechariah  
Baruch  
Ecclesiastes  
Tobit  
Esther  
Ezra  
Judith  
Nehemiah  
Wisdom of Solomon  
Daniel  
2 Maccabees  
Judges  
Joshua  
2 Samuel  
2 Kings  
1 Samuel  
1 Kings  
2 Chronicles  
Leviticus  
Proverbs  
1 Maccabees  
1 Chronicles  
Deuteronomy  
Job  
Exodus  
Ezekiel  
Numbers  
Isaiah  
Jeremiah  
Sirach/Ecclesiasticus  
Genesis  
Psalms  

Eventually once the [1935 Rahlfs LXX](https://el.wikisource.org/wiki/%CE%97_%CE%A0%CE%B1%CE%BB%CE%B1%CE%B9%CE%AC_%CE%94%CE%B9%CE%B1%CE%B8%CE%AE%CE%BA%CE%B7_(Rahlfs)) enters the Public Domain, a goal of this or a future project would be to conform the text to the more reputable Rahlfs LXX edition.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Thank you to all of the contributors to this project! May the Lord God remember you in his kingdom always, now and ever, and unto ages of ages, amen.

## License
This project is licensed under the Creative Commons Zero v1.0 Universal.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
